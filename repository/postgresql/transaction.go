package postgresql

import (
	"context"
	"database/sql"
	"mini-wallet-exercise/domain"
)

type TransactionRepository interface {
	Create(ctx context.Context, trans *domain.Transaction) error
}

type pgsqlTransactionRepository struct {
	db *sql.DB
}

func (t *pgsqlTransactionRepository) Create(ctx context.Context, trans *domain.Transaction) error {
	query := `INSERT INTO transactions (id, transaction_by, status, created_at, type, amount, reference_id) VALUES ($1, $2,$3, $4,$5, $6,$7)`
	stmt, err := t.db.PrepareContext(ctx, query)
	if err != nil {
		return err
	}
	defer stmt.Close()

	_, err = stmt.ExecContext(ctx, trans.ID, trans.CustomerXid, trans.Status, trans.CreatedAt, trans.Type, trans.Amount, trans.ReferenceID)

	if err != nil {
		return err
	}
	return nil
}

func NewPgsqlTransactionRepository(db *sql.DB) TransactionRepository {
	return &pgsqlTransactionRepository{
		db: db,
	}
}
