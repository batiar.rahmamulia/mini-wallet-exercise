package postgresql

import (
	"context"
	"database/sql"
	"github.com/google/uuid"
	"mini-wallet-exercise/domain"
)

type WalletRepository interface {
	Create(ctx context.Context, id uuid.UUID, cXid uuid.UUID) error
	Update(ctx context.Context, wallet *domain.Wallet) error
	Enable(ctx context.Context, wallet *domain.Wallet) error
	Disable(ctx context.Context, wallet *domain.Wallet) error
	GetByCustomerID(ctx context.Context, cXid uuid.UUID) (domain.Wallet, error)
	GetByID(ctx context.Context, id uuid.UUID) (domain.Wallet, error)
}

type pgsqlWalletRepository struct {
	db *sql.DB
}

func NewPgsqlWalletRepository(db *sql.DB) WalletRepository {
	return &pgsqlWalletRepository{
		db: db,
	}
}

func (w *pgsqlWalletRepository) Create(ctx context.Context, id uuid.UUID, cXid uuid.UUID) error {
	query := `INSERT INTO accounts (id, owned_by) VALUES ($1, $2)`
	stmt, err := w.db.PrepareContext(ctx, query)
	if err != nil {
		return err
	}
	defer stmt.Close()

	_, err = stmt.ExecContext(ctx, id, cXid)
	if err != nil {
		return err
	}
	return nil
}

func (w *pgsqlWalletRepository) Update(ctx context.Context, wallet *domain.Wallet) error {
	query := `UPDATE accounts SET balance=$1 WHERE id=$2`
	stmt, err := w.db.PrepareContext(ctx, query)
	if err != nil {
		return err
	}
	defer stmt.Close()

	_, err = stmt.ExecContext(ctx, wallet.Balance, wallet.ID)
	if err != nil {
		return err
	}
	return nil
}

func (w *pgsqlWalletRepository) Enable(ctx context.Context, wallet *domain.Wallet) error {
	query := `UPDATE accounts SET status=$1, enabled_at=$2, disabled_at=null WHERE id=$3`
	stmt, err := w.db.PrepareContext(ctx, query)
	if err != nil {
		return err
	}
	defer stmt.Close()

	_, err = stmt.ExecContext(ctx, wallet.Status, wallet.EnabledAt, wallet.ID)
	if err != nil {
		return err
	}
	return nil
}

func (w *pgsqlWalletRepository) Disable(ctx context.Context, wallet *domain.Wallet) error {
	query := `UPDATE accounts SET status=$1, disabled_at=$2, enabled_at=null WHERE id=$3`
	stmt, err := w.db.PrepareContext(ctx, query)
	if err != nil {
		return err
	}
	defer stmt.Close()

	_, err = stmt.ExecContext(ctx, wallet.Status, wallet.DisabledAt, wallet.ID)
	if err != nil {
		return err
	}
	return nil
}

func (w *pgsqlWalletRepository) GetByCustomerID(ctx context.Context, cXid uuid.UUID) (domain.Wallet, error) {
	var wallet domain.Wallet

	query := `SELECT id, owned_by, status, enabled_at, disabled_at, balance FROM accounts WHERE owned_by=$1`

	stmt, err := w.db.PrepareContext(ctx, query)
	if err != nil {
		return domain.Wallet{}, err
	}
	defer stmt.Close()

	row := w.db.QueryRowContext(ctx, query, cXid)

	err = row.Scan(&wallet.ID, &wallet.OwnedBy, &wallet.Status, &wallet.EnabledAt, &wallet.DisabledAt, &wallet.Balance)

	return wallet, err
}

func (w *pgsqlWalletRepository) GetByID(ctx context.Context, cXid uuid.UUID) (domain.Wallet, error) {
	var wallet domain.Wallet

	query := `SELECT id, owned_by, status, enabled_at, disabled_at, balance FROM accounts WHERE id=$1`

	stmt, err := w.db.PrepareContext(ctx, query)
	if err != nil {
		return domain.Wallet{}, err
	}
	defer stmt.Close()

	row := w.db.QueryRowContext(ctx, query, cXid)

	err = row.Scan(&wallet.ID, &wallet.OwnedBy, &wallet.Status, &wallet.EnabledAt, &wallet.DisabledAt, &wallet.Balance)

	return wallet, err
}
