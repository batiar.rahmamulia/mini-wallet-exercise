package domain

import (
	"context"
	"github.com/go-playground/validator/v10"
	"github.com/google/uuid"
)

type Account struct {
	CustomerXid uuid.UUID `json:"customer_xid" validate:"required"`
}

func (a *Account) Validate() map[string]string {
	validate := validator.New()
	err := validate.Struct(a)
	errors := make(map[string]string)
	if err != nil {
		for _, err := range err.(validator.ValidationErrors) {
			errors[err.Field()] = err.ActualTag()
		}
		return errors
	}

	return nil
}

type AccountUsecase interface {
	Create(ctx context.Context, account *Account) (token string, err error)
}
