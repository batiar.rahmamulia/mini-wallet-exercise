package domain

import (
	"context"
	"github.com/go-playground/validator/v10"
	"time"

	"github.com/google/uuid"
)

type Wallet struct {
	ID         uuid.UUID  `json:"id"`
	OwnedBy    uuid.UUID  `json:"owned_by"`
	Status     string     `json:"status"`
	EnabledAt  *time.Time `json:"enabled_at"`
	DisabledAt *time.Time `json:"disabled_at"`
	Balance    int        `json:"balance"`
}

func (a *Wallet) Validate() map[string]string {
	validate := validator.New()
	err := validate.Struct(a)
	errors := make(map[string]string)
	if err != nil {
		for _, err := range err.(validator.ValidationErrors) {
			errors[err.Field()] = err.ActualTag()
		}
		return errors
	}

	return nil
}

type WalletUsecase interface {
	Enable(ctx context.Context, id uuid.UUID) (Wallet, error)
	Disable(ctx context.Context, id uuid.UUID) (Wallet, error)
	GetByID(ctx context.Context, id uuid.UUID) (Wallet, error)
}

type WalletRepository interface {
	Create(ctx context.Context, id uuid.UUID, cXid uuid.UUID) error
	Update(ctx context.Context, wallet *Wallet) error
	Enable(ctx context.Context, wallet *Wallet) error
	Disable(ctx context.Context, wallet *Wallet) error
	GetByCustomerID(ctx context.Context, cXid uuid.UUID) (Wallet, error)
	GetByID(ctx context.Context, id uuid.UUID) (Wallet, error)
}
