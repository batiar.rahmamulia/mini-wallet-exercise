package domain

import (
	"context"
	"github.com/go-playground/validator/v10"
	"github.com/google/uuid"
	"time"
)

type Transaction struct {
	ID          uuid.UUID  `json:"id"`
	CustomerXid uuid.UUID  `json:"customer_xid"`
	Status      string     `json:"status"`
	CreatedAt   *time.Time `json:"created_at"`
	Type        string     `json:"type"`
	Amount      int        `json:"amount" validate:"required"`
	ReferenceID uuid.UUID  `json:"reference_id" validate:"required"`
}

func (a *Transaction) Validate() map[string]string {
	validate := validator.New()
	err := validate.Struct(a)
	errors := make(map[string]string)
	if err != nil {
		for _, err := range err.(validator.ValidationErrors) {
			errors[err.Field()] = err.ActualTag()
		}
		return errors
	}

	return nil
}

type TransactionUsecase interface {
	Deposit(ctx context.Context, id uuid.UUID, trans *Transaction) error
	Withdrawal(ctx context.Context, id uuid.UUID, trans *Transaction) error
}

type TransactionRepository interface {
	Create(ctx context.Context, trans *Transaction) error
}
