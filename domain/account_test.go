package domain_test

import (
	"fmt"
	"github.com/google/uuid"
	"github.com/stretchr/testify/assert"
	"mini-wallet-exercise/domain"
	"testing"
)

func TestAccountValidate(t *testing.T) {
	tests := []struct {
		description string
		account     domain.Account
		expectedRes map[string]string
	}{
		{
			description: "return empty",
			account: domain.Account{
				CustomerXid: uuid.New(),
			},
			expectedRes: nil,
		},
		{
			description: "return map",
			account:     domain.Account{},
			expectedRes: map[string]string{"CustomerXid": "required"},
		},
	}

	for _, test := range tests {
		t.Run(fmt.Sprintf("%s", test.description), func(t *testing.T) {
			m := test.account.Validate()
			assert.Equalf(t, test.expectedRes, m, test.description)
		})
	}
}
