package domain_test

import (
	"fmt"
	"github.com/google/uuid"
	"github.com/stretchr/testify/assert"
	"mini-wallet-exercise/domain"
	"testing"
)

func TestTransactionValidate(t *testing.T) {
	tests := []struct {
		description string
		transaction domain.Transaction
		expectedRes map[string]string
	}{
		{
			description: "return empty",
			transaction: domain.Transaction{
				ID:          uuid.UUID{},
				CustomerXid: uuid.New(),
				Status:      "",
				CreatedAt:   nil,
				Type:        "withdrawal",
				Amount:      10,
				ReferenceID: uuid.New(),
			},
			expectedRes: nil,
		},
		{
			description: "return map",
			transaction: domain.Transaction{},
			expectedRes: map[string]string{
				"Amount":      "required",
				"ReferenceID": "required",
			},
		},
	}

	for _, test := range tests {
		t.Run(fmt.Sprintf("%s", test.description), func(t *testing.T) {
			m := test.transaction.Validate()
			assert.Equalf(t, test.expectedRes, m, test.description)
		})
	}
}
