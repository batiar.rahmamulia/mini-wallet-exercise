package domain_test

import (
	"fmt"
	"github.com/stretchr/testify/assert"
	"mini-wallet-exercise/domain"
	"testing"
)

func TestWalletValidate(t *testing.T) {
	tests := []struct {
		description string
		wallet      domain.Wallet
		expectedRes map[string]string
	}{
		{
			description: "return empty",
			wallet:      domain.Wallet{},
			expectedRes: nil,
		},
	}

	for _, test := range tests {
		t.Run(fmt.Sprintf("%s", test.description), func(t *testing.T) {
			m := test.wallet.Validate()
			assert.Equalf(t, test.expectedRes, m, test.description)
		})
	}
}
