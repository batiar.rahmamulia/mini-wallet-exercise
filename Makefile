define GetFromCfg
$(shell node -p "require('./config/config.json').$(1)")
endef

USER		:= $(call GetFromCfg,database.user)
PASSWORD	:= $(call GetFromCfg,database.pass)
DBNAME		:= $(call GetFromCfg,database.name)
PORT		:= $(call GetFromCfg,database.port)
HOST		:= $(call GetFromCfg,database.host)

.PHONY: migrate_up
migrate_up:  ## Up migration
	migrate -database postgres://$(USER):$(PASSWORD)@$(HOST):$(PORT)/$(DBNAME)?sslmode=disable -path migration up

.PHONY: migrate_down
migrate_down: ## Down migration
	migrate -database postgres://$(USER):$(PASSWORD)@$(HOST):$(PORT)/$(DBNAME)?sslmode=disable -path migration down

.PHONY: migrate_drop
migrate_drop:  ## Drop migration
	migrate -database postgres://$(USER):$(PASSWORD)@$(HOST):$(PORT)/$(DBNAME)?sslmode=disable -path migration drop

.PHONY: tidy
tidy: ## Run golang mod tidy
	go mod tidy

.PHONY: api
api: ## Run api server
	go run cmd/api/main.go

.PHONY: build
build:
	go build -o ./wallet_api cmd/api/*.go

.PHONY: test
test: ## Run all test
	echo 'mode: atomic' > coverage.out && go test -covermode=atomic -coverprofile=coverage.out -v -race -timeout=30s ./...

.PHONY: e2e
e2e: ## Run e2e test
	./test/e2e/e2e-testing.sh

.PHONY: cover
cover: test ## Run all the tests and opens the coverage report
	go tool cover -html=coverage.out

.PHONY: lint_prepare
lint_prepare: ## Installing golangci-lint
	curl -sfL https://raw.githubusercontent.com/golangci/golangci-lint/master/install.sh| sh -s latest

.PHONY: lint
lint: ## Run golangci lint
	golangci-lint run --enable-all

.PHONY: k6_all
k6_all: ## Run load test all endpoint using k6
	k6 run --vus 1 --iterations 1 test/load/all_flow.js

.PHONY: docker
docker:
	docker build -t mini-wallet-exercise .

.PHONY: docker_run
docker_run:
	docker-compose up --build -d

.PHONY: docker_stop
docker_stop:
	docker-compose down

.PHONY: help
help:
	@grep -E '^[a-zA-Z_-]+:.*?## .*$$' $(MAKEFILE_LIST) | awk 'BEGIN {FS = ":.*?## "}; {printf "\033[36m%-30s\033[0m %s\n", $$1, $$2}'
