package main

import (
	"fmt"
	"github.com/gofiber/fiber/v2"
	"github.com/gofiber/fiber/v2/middleware/compress"
	"github.com/gofiber/fiber/v2/middleware/cors"
	"github.com/gofiber/fiber/v2/middleware/limiter"
	"github.com/gofiber/fiber/v2/middleware/logger"
	"github.com/gofiber/fiber/v2/middleware/recover"
	_ "github.com/lib/pq"
	"log"
	"mini-wallet-exercise/config"
	"mini-wallet-exercise/delivery/rest"
	"mini-wallet-exercise/repository/postgresql"
	"mini-wallet-exercise/usecase"
	"time"
)

func main() {
	cfg := config.Init()
	dbConn, err := config.Connect(cfg)
	if err != nil {
		log.Fatalf("Cannot initialize connection to database: %v", err)
	}

	defer func() {
		err := dbConn.Close()
		if err != nil {
			log.Fatal(err)
		}
	}()

	app := fiber.New()
	app.Use(recover.New())
	app.Use(cors.New())
	app.Use(compress.New())
	app.Use(limiter.New(limiter.Config{
		Max: 100,
		LimitReached: func(c *fiber.Ctx) error {
			return c.Status(fiber.StatusTooManyRequests).JSON(&fiber.Map{
				"status":  "fail",
				"message": "You have requested too many in a single time-frame! Please wait another minute!",
			})
		},
	}))
	app.Use(logger.New())
	rest.NewMiscHandler(app)

	timeoutContext := time.Duration(cfg.Context.Timeout) * time.Second

	walletRepo := postgresql.NewPgsqlWalletRepository(dbConn)
	walletUsecase := usecase.NewWalletUsecase(walletRepo, timeoutContext)
	rest.NewWalletHandler(app, walletUsecase)

	accountUsecase := usecase.NewAccountUsecase(walletRepo, timeoutContext, cfg)
	rest.NewAccountHandler(app, accountUsecase)

	transactionRepo := postgresql.NewPgsqlTransactionRepository(dbConn)
	transactionUsecase := usecase.NewTransactionUsecase(walletRepo, transactionRepo, timeoutContext)
	rest.NewTransactionHandler(app, transactionUsecase)

	app.All("*", func(c *fiber.Ctx) error {
		errorMessage := fmt.Sprintf("Route '%s' does not exist in this API!", c.OriginalURL())

		return c.Status(fiber.StatusNotFound).JSON(&fiber.Map{
			"status":  "fail",
			"message": errorMessage,
		})
	})

	log.Fatal(app.Listen(cfg.Server.Address))
}
