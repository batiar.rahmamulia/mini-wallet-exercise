package usecase

import (
	"context"
	"github.com/google/uuid"
	"mini-wallet-exercise/delivery/rest/util"
	"mini-wallet-exercise/domain"
	"time"
)

type transactionUsecase struct {
	walletRepo      domain.WalletRepository
	transactionRepo domain.TransactionRepository
	contextTimeout  time.Duration
}

func NewTransactionUsecase(walletRepo domain.WalletRepository, transactionRepo domain.TransactionRepository, timeout time.Duration) domain.TransactionUsecase {
	return &transactionUsecase{
		walletRepo:      walletRepo,
		transactionRepo: transactionRepo,
		contextTimeout:  timeout,
	}
}

func (t *transactionUsecase) Deposit(ctx context.Context, id uuid.UUID, trans *domain.Transaction) error {
	wallet, err := t.walletRepo.GetByID(ctx, id)
	if err != nil {
		return util.NewClientError(map[string]string{"id": "not found"})
	}
	now := time.Now()

	if wallet.Status == StatusDisabled {
		return util.NewClientError(map[string]string{"status": "disabled"})
	}
	wallet.Balance += trans.Amount
	err = t.walletRepo.Update(ctx, &wallet)
	if err != nil {
		return util.NewServerError(err.Error())
	}

	trans.ID = uuid.New()
	trans.Status = "success"
	trans.Type = "deposit"
	trans.CreatedAt = &now
	trans.CustomerXid = wallet.OwnedBy

	err = t.transactionRepo.Create(ctx, trans)
	if err != nil {
		return util.NewServerError(err.Error())
	}

	return nil
}
func (t *transactionUsecase) Withdrawal(ctx context.Context, id uuid.UUID, trans *domain.Transaction) error {
	wallet, err := t.walletRepo.GetByID(ctx, id)
	if err != nil {
		return util.NewClientError(map[string]string{"id": "not found"})
	}
	now := time.Now()

	if wallet.Status == StatusDisabled {
		return util.NewClientError(map[string]string{"status": "disabled"})
	}
	if wallet.Balance < trans.Amount {
		return util.NewClientError(map[string]string{"balance": "insufficient"})
	}
	wallet.Balance -= trans.Amount
	err = t.walletRepo.Update(ctx, &wallet)
	if err != nil {
		return util.NewServerError(err.Error())
	}

	trans.ID = uuid.New()
	trans.Status = "success"
	trans.Type = "withdrawal"
	trans.CreatedAt = &now
	trans.CustomerXid = wallet.OwnedBy

	err = t.transactionRepo.Create(ctx, trans)
	if err != nil {
		return util.NewServerError(err.Error())
	}

	return nil
}
