package usecase

import (
	"context"
	"database/sql"
	"github.com/google/uuid"
	"mini-wallet-exercise/config"
	"mini-wallet-exercise/delivery/rest/util"
	"time"

	"github.com/golang-jwt/jwt/v4"
	"mini-wallet-exercise/domain"
)

type accountUsecase struct {
	walletRepo     domain.WalletRepository
	contextTimeout time.Duration
	config         *config.Config
}

func (a *accountUsecase) Create(ctx context.Context, account *domain.Account) (token string, err error) {
	wallet, err := a.walletRepo.GetByCustomerID(ctx, account.CustomerXid)
	if err != nil && err != sql.ErrNoRows {
		return "", util.NewServerError(err.Error())
	}

	if wallet.ID == uuid.Nil {
		wallet.ID = uuid.New()
		err = a.walletRepo.Create(ctx, wallet.ID, account.CustomerXid)
		if err != nil {
			return "", util.NewServerError(err.Error())
		}
	}

	claims := jwt.MapClaims{
		"ID":          wallet.ID,
		"customerXid": account.CustomerXid,
		"exp":         time.Now().Add(time.Hour * 72).Unix(),
	}
	t := jwt.NewWithClaims(jwt.SigningMethodHS256, claims)

	token, err = t.SignedString([]byte(a.config.JWT.SecretKey))
	if err != nil {
		return "", util.NewServerError(err.Error())
	}

	if token == "" {
		return "", util.NewServerError("something error, cannot generate token")
	}

	return
}

func NewAccountUsecase(walletRepo domain.WalletRepository, timeout time.Duration, cfg *config.Config) domain.AccountUsecase {
	return &accountUsecase{
		walletRepo:     walletRepo,
		contextTimeout: timeout,
		config:         cfg,
	}
}
