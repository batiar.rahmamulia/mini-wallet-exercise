package usecase_test

import (
	"context"
	"fmt"
	"github.com/golang/mock/gomock"
	"github.com/google/uuid"
	"github.com/stretchr/testify/assert"
	"mini-wallet-exercise/delivery/rest/util"
	"mini-wallet-exercise/domain"
	mock_domain "mini-wallet-exercise/test/mock/domain/mock_wallet"
	mock_repository "mini-wallet-exercise/test/mock/repository/mock_postgresql/transaction/postgresql"
	"mini-wallet-exercise/usecase"
	"testing"
	"time"
)

func TestTransactionUsecaseDeposit(t *testing.T) {
	tests := []struct {
		description string
		req         struct {
			id    uuid.UUID
			trans *domain.Transaction
		}
		expectedRes struct {
			err error
		}
		mockRepo struct {
			walletGetByCID domain.Wallet
			errGetByCID    error
			callGetByCID   int
			errUpdate      error
			callUpdate     int
			errCreate      error
			callCreate     int
		}
	}{
		{
			description: "error wallet repo GetByCID",
			req: struct {
				id    uuid.UUID
				trans *domain.Transaction
			}{id: uuid.New(), trans: &domain.Transaction{}},
			expectedRes: struct{ err error }{err: &util.ClientError{Data: map[string]string{"id": "not found"}}},
			mockRepo: struct {
				walletGetByCID domain.Wallet
				errGetByCID    error
				callGetByCID   int
				errUpdate      error
				callUpdate     int
				errCreate      error
				callCreate     int
			}{
				walletGetByCID: domain.Wallet{},
				errGetByCID:    fmt.Errorf("error"),
				callGetByCID:   1,
				errUpdate:      nil,
				callUpdate:     0,
				errCreate:      nil,
				callCreate:     0,
			},
		},
		{
			description: "error wallet repo Update",
			req: struct {
				id    uuid.UUID
				trans *domain.Transaction
			}{id: uuid.New(), trans: &domain.Transaction{}},
			expectedRes: struct{ err error }{err: &util.ServerError{Message: "error"}},
			mockRepo: struct {
				walletGetByCID domain.Wallet
				errGetByCID    error
				callGetByCID   int
				errUpdate      error
				callUpdate     int
				errCreate      error
				callCreate     int
			}{
				walletGetByCID: domain.Wallet{},
				errGetByCID:    nil,
				callGetByCID:   1,
				errUpdate:      fmt.Errorf("error"),
				callUpdate:     1,
				errCreate:      nil,
				callCreate:     0,
			},
		},
		{
			description: "error transaction repo Create",
			req: struct {
				id    uuid.UUID
				trans *domain.Transaction
			}{id: uuid.New(), trans: &domain.Transaction{}},
			expectedRes: struct{ err error }{err: &util.ServerError{Message: "error"}},
			mockRepo: struct {
				walletGetByCID domain.Wallet
				errGetByCID    error
				callGetByCID   int
				errUpdate      error
				callUpdate     int
				errCreate      error
				callCreate     int
			}{
				walletGetByCID: domain.Wallet{},
				errGetByCID:    nil,
				callGetByCID:   1,
				errUpdate:      nil,
				callUpdate:     1,
				errCreate:      fmt.Errorf("error"),
				callCreate:     1,
			},
		},
	}

	mockCtrl := gomock.NewController(t)
	mockWalletRepo := mock_domain.NewMockWalletRepository(mockCtrl)
	mockTransactionRepo := mock_repository.NewMockTransactionRepository(mockCtrl)
	tus := usecase.NewTransactionUsecase(mockWalletRepo, mockTransactionRepo, 5*time.Second)

	for _, test := range tests {
		t.Run(fmt.Sprintf("%s", test.description), func(t *testing.T) {
			mockWalletRepo.EXPECT().GetByID(gomock.Any(), gomock.Any()).Return(test.mockRepo.walletGetByCID, test.mockRepo.errGetByCID).Times(test.mockRepo.callGetByCID)

			mockWalletRepo.EXPECT().Update(gomock.Any(), gomock.Any()).Return(test.mockRepo.errUpdate).Times(test.mockRepo.callUpdate)

			mockTransactionRepo.EXPECT().Create(gomock.Any(), gomock.Any()).Return(test.mockRepo.errCreate).Times(test.mockRepo.callCreate)

			err := tus.Deposit(context.Background(), test.req.id, test.req.trans)
			assert.Equal(t, err, test.expectedRes.err)
		})
	}
}

func TestTransactionUsecaseWithdrawal(t *testing.T) {
	tests := []struct {
		description string
		req         struct {
			id    uuid.UUID
			trans *domain.Transaction
		}
		expectedRes struct {
			err error
		}
		mockRepo struct {
			walletGetByCID domain.Wallet
			errGetByCID    error
			callGetByCID   int
			errUpdate      error
			callUpdate     int
			errCreate      error
			callCreate     int
		}
	}{
		{
			description: "error wallet repo GetByCID",
			req: struct {
				id    uuid.UUID
				trans *domain.Transaction
			}{id: uuid.New(), trans: &domain.Transaction{}},
			expectedRes: struct{ err error }{err: &util.ClientError{Data: map[string]string{"id": "not found"}}},
			mockRepo: struct {
				walletGetByCID domain.Wallet
				errGetByCID    error
				callGetByCID   int
				errUpdate      error
				callUpdate     int
				errCreate      error
				callCreate     int
			}{
				walletGetByCID: domain.Wallet{},
				errGetByCID:    fmt.Errorf("error"),
				callGetByCID:   1,
				errUpdate:      nil,
				callUpdate:     0,
				errCreate:      nil,
				callCreate:     0,
			},
		},
		{
			description: "error insufficent balance",
			req: struct {
				id    uuid.UUID
				trans *domain.Transaction
			}{id: uuid.New(), trans: &domain.Transaction{Amount: 100}},
			expectedRes: struct{ err error }{err: &util.ClientError{Data: map[string]string{"balance": "insufficient"}}},
			mockRepo: struct {
				walletGetByCID domain.Wallet
				errGetByCID    error
				callGetByCID   int
				errUpdate      error
				callUpdate     int
				errCreate      error
				callCreate     int
			}{
				walletGetByCID: domain.Wallet{},
				errGetByCID:    nil,
				callGetByCID:   1,
				errUpdate:      nil,
				callUpdate:     0,
				errCreate:      nil,
				callCreate:     0,
			},
		},
		{
			description: "error wallet repo Update",
			req: struct {
				id    uuid.UUID
				trans *domain.Transaction
			}{id: uuid.New(), trans: &domain.Transaction{}},
			expectedRes: struct{ err error }{err: &util.ServerError{Message: "error"}},
			mockRepo: struct {
				walletGetByCID domain.Wallet
				errGetByCID    error
				callGetByCID   int
				errUpdate      error
				callUpdate     int
				errCreate      error
				callCreate     int
			}{
				walletGetByCID: domain.Wallet{},
				errGetByCID:    nil,
				callGetByCID:   1,
				errUpdate:      fmt.Errorf("error"),
				callUpdate:     1,
				errCreate:      nil,
				callCreate:     0,
			},
		},
		{
			description: "error transaction repo Create",
			req: struct {
				id    uuid.UUID
				trans *domain.Transaction
			}{id: uuid.New(), trans: &domain.Transaction{}},
			expectedRes: struct{ err error }{err: &util.ServerError{Message: "error"}},
			mockRepo: struct {
				walletGetByCID domain.Wallet
				errGetByCID    error
				callGetByCID   int
				errUpdate      error
				callUpdate     int
				errCreate      error
				callCreate     int
			}{
				walletGetByCID: domain.Wallet{},
				errGetByCID:    nil,
				callGetByCID:   1,
				errUpdate:      nil,
				callUpdate:     1,
				errCreate:      fmt.Errorf("error"),
				callCreate:     1,
			},
		},
	}

	mockCtrl := gomock.NewController(t)
	mockWalletRepo := mock_domain.NewMockWalletRepository(mockCtrl)
	mockTransactionRepo := mock_repository.NewMockTransactionRepository(mockCtrl)
	tus := usecase.NewTransactionUsecase(mockWalletRepo, mockTransactionRepo, 5*time.Second)

	for _, test := range tests {
		t.Run(fmt.Sprintf("%s", test.description), func(t *testing.T) {
			mockWalletRepo.EXPECT().GetByID(gomock.Any(), gomock.Any()).Return(test.mockRepo.walletGetByCID, test.mockRepo.errGetByCID).Times(test.mockRepo.callGetByCID)

			mockWalletRepo.EXPECT().Update(gomock.Any(), gomock.Any()).Return(test.mockRepo.errUpdate).Times(test.mockRepo.callUpdate)

			mockTransactionRepo.EXPECT().Create(gomock.Any(), gomock.Any()).Return(test.mockRepo.errCreate).Times(test.mockRepo.callCreate)

			err := tus.Withdrawal(context.Background(), test.req.id, test.req.trans)
			assert.Equal(t, err, test.expectedRes.err)
		})
	}
}
