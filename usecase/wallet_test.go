package usecase_test

import (
	"context"
	"database/sql"
	"fmt"
	"github.com/golang/mock/gomock"
	"github.com/google/uuid"
	"github.com/stretchr/testify/assert"
	"mini-wallet-exercise/delivery/rest/util"
	"mini-wallet-exercise/domain"
	mock_domain "mini-wallet-exercise/test/mock/domain/mock_wallet"
	"mini-wallet-exercise/usecase"
	"testing"
	"time"
)

func TestWalletUsecaseGetByID(t *testing.T) {
	tests := []struct {
		description string
		req         struct {
			id uuid.UUID
		}
		expectedRes struct {
			wallet domain.Wallet
			err    error
		}
		mockWalletRepo struct {
			walletGetByID domain.Wallet
			errGetByID    error
			callGetByID   int
		}
	}{
		{
			description: "error wallet repo GetByID",
			req:         struct{ id uuid.UUID }{id: uuid.New()},
			expectedRes: struct {
				wallet domain.Wallet
				err    error
			}{
				wallet: domain.Wallet{
					ID:         uuid.UUID{},
					OwnedBy:    uuid.UUID{},
					Status:     "",
					EnabledAt:  nil,
					DisabledAt: nil,
					Balance:    0,
				},
				err: &util.ServerError{Message: "error"},
			},
			mockWalletRepo: struct {
				walletGetByID domain.Wallet
				errGetByID    error
				callGetByID   int
			}{
				walletGetByID: domain.Wallet{},
				errGetByID:    fmt.Errorf("error"),
				callGetByID:   1,
			},
		},
		{
			description: "success",
			req:         struct{ id uuid.UUID }{id: uuid.New()},
			expectedRes: struct {
				wallet domain.Wallet
				err    error
			}{
				wallet: domain.Wallet{
					ID:         uuid.UUID{},
					OwnedBy:    uuid.UUID{},
					Status:     "",
					EnabledAt:  nil,
					DisabledAt: nil,
					Balance:    0,
				},
				err: nil,
			},
			mockWalletRepo: struct {
				walletGetByID domain.Wallet
				errGetByID    error
				callGetByID   int
			}{
				walletGetByID: domain.Wallet{},
				errGetByID:    nil,
				callGetByID:   1,
			},
		},
	}

	mockCtrl := gomock.NewController(t)
	mockWalletRepo := mock_domain.NewMockWalletRepository(mockCtrl)
	wus := usecase.NewWalletUsecase(mockWalletRepo, 5*time.Second)

	for _, test := range tests {
		t.Run(fmt.Sprintf("%s", test.description), func(t *testing.T) {
			mockWalletRepo.EXPECT().GetByID(gomock.Any(), gomock.Any()).Return(test.mockWalletRepo.walletGetByID, test.mockWalletRepo.errGetByID).Times(test.mockWalletRepo.callGetByID)

			wallet, err := wus.GetByID(context.Background(), uuid.New())
			assert.Equal(t, err, test.expectedRes.err)
			assert.Equal(t, wallet, test.expectedRes.wallet)
		})
	}
}

func TestWalletUsecaseDisable(t *testing.T) {
	tests := []struct {
		description string
		req         struct {
			id uuid.UUID
		}
		expectedRes struct {
			wallet domain.Wallet
			err    error
		}
		mockWalletRepo struct {
			walletGetByID domain.Wallet
			errGetByID    error
			callGetByID   int
			errDisable    error
			callDisable   int
		}
	}{
		{
			description: "error wallet repo client GetByID",
			req:         struct{ id uuid.UUID }{id: uuid.New()},
			expectedRes: struct {
				wallet domain.Wallet
				err    error
			}{
				wallet: domain.Wallet{
					ID:         uuid.UUID{},
					OwnedBy:    uuid.UUID{},
					Status:     "",
					EnabledAt:  nil,
					DisabledAt: nil,
					Balance:    0,
				},
				err: &util.ClientError{Data: map[string]string{"id": "wallet not found"}},
			},
			mockWalletRepo: struct {
				walletGetByID domain.Wallet
				errGetByID    error
				callGetByID   int
				errDisable    error
				callDisable   int
			}{
				walletGetByID: domain.Wallet{},
				errGetByID:    sql.ErrNoRows,
				callGetByID:   1,
				errDisable:    nil,
				callDisable:   0,
			},
		},
		{
			description: "error wallet repo server GetByID",
			req:         struct{ id uuid.UUID }{id: uuid.New()},
			expectedRes: struct {
				wallet domain.Wallet
				err    error
			}{
				wallet: domain.Wallet{
					ID:         uuid.UUID{},
					OwnedBy:    uuid.UUID{},
					Status:     "",
					EnabledAt:  nil,
					DisabledAt: nil,
					Balance:    0,
				},
				err: &util.ServerError{Message: "error"},
			},
			mockWalletRepo: struct {
				walletGetByID domain.Wallet
				errGetByID    error
				callGetByID   int
				errDisable    error
				callDisable   int
			}{
				walletGetByID: domain.Wallet{},
				errGetByID:    fmt.Errorf("error"),
				callGetByID:   1,
				errDisable:    nil,
				callDisable:   0,
			},
		},
		{
			description: "error status already disabled",
			req:         struct{ id uuid.UUID }{id: uuid.New()},
			expectedRes: struct {
				wallet domain.Wallet
				err    error
			}{
				wallet: domain.Wallet{
					ID:         uuid.UUID{},
					OwnedBy:    uuid.UUID{},
					Status:     "",
					EnabledAt:  nil,
					DisabledAt: nil,
					Balance:    0,
				},
				err: &util.ClientError{Data: map[string]string{"status": "status already disabled"}},
			},
			mockWalletRepo: struct {
				walletGetByID domain.Wallet
				errGetByID    error
				callGetByID   int
				errDisable    error
				callDisable   int
			}{
				walletGetByID: domain.Wallet{Status: usecase.StatusDisabled},
				errGetByID:    nil,
				callGetByID:   1,
				errDisable:    nil,
				callDisable:   0,
			},
		},
		{
			description: "error wallet repo Update",
			req:         struct{ id uuid.UUID }{id: uuid.New()},
			expectedRes: struct {
				wallet domain.Wallet
				err    error
			}{
				wallet: domain.Wallet{
					ID:         uuid.UUID{},
					OwnedBy:    uuid.UUID{},
					Status:     "",
					EnabledAt:  nil,
					DisabledAt: nil,
					Balance:    0,
				},
				err: &util.ServerError{Message: "error"},
			},
			mockWalletRepo: struct {
				walletGetByID domain.Wallet
				errGetByID    error
				callGetByID   int
				errDisable    error
				callDisable   int
			}{
				walletGetByID: domain.Wallet{},
				errGetByID:    nil,
				callGetByID:   1,
				errDisable:    fmt.Errorf("error"),
				callDisable:   1,
			},
		},
		{
			description: "success",
			req:         struct{ id uuid.UUID }{id: uuid.New()},
			expectedRes: struct {
				wallet domain.Wallet
				err    error
			}{
				wallet: domain.Wallet{
					ID:         uuid.UUID{},
					OwnedBy:    uuid.UUID{},
					Status:     usecase.StatusDisabled,
					EnabledAt:  nil,
					DisabledAt: nil,
					Balance:    0,
				},
				err: nil,
			},
			mockWalletRepo: struct {
				walletGetByID domain.Wallet
				errGetByID    error
				callGetByID   int
				errDisable    error
				callDisable   int
			}{
				walletGetByID: domain.Wallet{},
				errGetByID:    nil,
				callGetByID:   1,
				errDisable:    nil,
				callDisable:   1,
			},
		},
	}

	mockCtrl := gomock.NewController(t)
	mockWalletRepo := mock_domain.NewMockWalletRepository(mockCtrl)
	wus := usecase.NewWalletUsecase(mockWalletRepo, 5*time.Second)

	for _, test := range tests {
		t.Run(fmt.Sprintf("%s", test.description), func(t *testing.T) {
			mockWalletRepo.EXPECT().GetByID(gomock.Any(), gomock.Any()).Return(test.mockWalletRepo.walletGetByID, test.mockWalletRepo.errGetByID).Times(test.mockWalletRepo.callGetByID)

			mockWalletRepo.EXPECT().Disable(gomock.Any(), gomock.Any()).Return(test.mockWalletRepo.errDisable).Times(test.mockWalletRepo.callDisable)

			_, err := wus.Disable(context.Background(), uuid.New())
			assert.Equal(t, err, test.expectedRes.err)
		})
	}
}
