package usecase_test

import (
	"context"
	"fmt"
	"github.com/golang/mock/gomock"
	"github.com/google/uuid"
	"github.com/stretchr/testify/assert"
	"mini-wallet-exercise/config"
	"mini-wallet-exercise/delivery/rest/util"
	"mini-wallet-exercise/domain"
	mock_domain "mini-wallet-exercise/test/mock/domain/mock_wallet"
	"mini-wallet-exercise/usecase"
	"testing"
	"time"
)

func TestAccountUsecase(t *testing.T) {
	tests := []struct {
		description string
		req         struct {
			account *domain.Account
		}
		expectedRes struct {
			token bool
			err   error
		}
		mockWalletRepo struct {
			walletGetByCID domain.Wallet
			errGetByCID    error
			callGetByCID   int
			errCreate      error
			callCreate     int
		}
	}{
		{
			description: "error wallet repo GetByCID",
			req: struct {
				account *domain.Account
			}{
				account: nil,
			},
			expectedRes: struct {
				token bool
				err   error
			}{
				token: false,
				err:   &util.ServerError{Message: "error"},
			},
			mockWalletRepo: struct {
				walletGetByCID domain.Wallet
				errGetByCID    error
				callGetByCID   int
				errCreate      error
				callCreate     int
			}{
				walletGetByCID: domain.Wallet{},
				errGetByCID:    fmt.Errorf("error"),
				callGetByCID:   1,
				errCreate:      nil,
				callCreate:     0,
			},
		},
		{
			description: "error wallet repo GetByCID",
			req: struct {
				account *domain.Account
			}{
				account: nil,
			},
			expectedRes: struct {
				token bool
				err   error
			}{
				token: false,
				err:   &util.ServerError{Message: "error"},
			},
			mockWalletRepo: struct {
				walletGetByCID domain.Wallet
				errGetByCID    error
				callGetByCID   int
				errCreate      error
				callCreate     int
			}{
				walletGetByCID: domain.Wallet{},
				errGetByCID:    nil,
				callGetByCID:   1,
				errCreate:      fmt.Errorf("error"),
				callCreate:     1,
			},
		},
		{
			description: "error wallet repo GetByCID",
			req: struct {
				account *domain.Account
			}{
				account: nil,
			},
			expectedRes: struct {
				token bool
				err   error
			}{
				token: true,
				err:   nil,
			},
			mockWalletRepo: struct {
				walletGetByCID domain.Wallet
				errGetByCID    error
				callGetByCID   int
				errCreate      error
				callCreate     int
			}{
				walletGetByCID: domain.Wallet{},
				errGetByCID:    nil,
				callGetByCID:   1,
				errCreate:      nil,
				callCreate:     1,
			},
		},
	}

	mockCtrl := gomock.NewController(t)
	mockWalletRepo := mock_domain.NewMockWalletRepository(mockCtrl)
	dus := usecase.NewAccountUsecase(mockWalletRepo, 5*time.Second, &config.Config{
		JWT: config.JWT{
			SecretKey:                   "secret",
			SecretKeyExpireMinutesCount: 15,
		},
		Debug: false,
		Server: config.Server{
			Address: "address",
		},
		Context: config.Context{
			Timeout: 5,
		},
		Database: config.Database{
			Host:    "host",
			Port:    "port",
			User:    "user",
			Pass:    "pass",
			Name:    "name",
			SSLMode: "ssl",
		},
	})

	for _, test := range tests {
		t.Run(fmt.Sprintf("%s", test.description), func(t *testing.T) {
			mockWalletRepo.EXPECT().GetByCustomerID(gomock.Any(), gomock.Any()).Return(test.mockWalletRepo.walletGetByCID, test.mockWalletRepo.errGetByCID).Times(test.mockWalletRepo.callGetByCID)
			mockWalletRepo.EXPECT().Create(gomock.Any(), gomock.Any(), gomock.Any()).Return(test.mockWalletRepo.errCreate).Times(test.mockWalletRepo.callCreate)

			token, err := dus.Create(context.Background(), &domain.Account{CustomerXid: uuid.New()})
			assert.Equal(t, err, test.expectedRes.err)
			if test.expectedRes.token {
				assert.NotNil(t, token)
			}
		})
	}
}
