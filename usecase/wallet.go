package usecase

import (
	"context"
	"database/sql"
	"github.com/google/uuid"
	"mini-wallet-exercise/delivery/rest/util"
	"time"

	"mini-wallet-exercise/domain"
)

const (
	StatusEnabled  = "enabled"
	StatusDisabled = "disabled"
)

type walletUsecase struct {
	walletRepo     domain.WalletRepository
	contextTimeout time.Duration
}

func (w *walletUsecase) Enable(ctx context.Context, id uuid.UUID) (domain.Wallet, error) {
	wallet, err := w.walletRepo.GetByID(ctx, id)
	if err != nil {
		if err == sql.ErrNoRows {
			return domain.Wallet{}, util.NewClientError(map[string]string{"id": "wallet not found"})
		}
		return domain.Wallet{}, util.NewServerError(err.Error())
	}

	if wallet.Status == StatusEnabled {
		return domain.Wallet{}, util.NewClientError(map[string]string{"status": "status already enabled"})
	}

	wallet.Status = StatusEnabled
	now := time.Now()
	wallet.EnabledAt = &now
	wallet.DisabledAt = nil
	err = w.walletRepo.Enable(ctx, &wallet)
	if err != nil {
		return domain.Wallet{}, util.NewServerError(err.Error())
	}

	return wallet, err

}

func (w *walletUsecase) Disable(ctx context.Context, id uuid.UUID) (domain.Wallet, error) {
	wallet, err := w.walletRepo.GetByID(ctx, id)
	if err != nil {
		if err == sql.ErrNoRows {
			return domain.Wallet{}, util.NewClientError(map[string]string{"id": "wallet not found"})
		}
		return domain.Wallet{}, util.NewServerError(err.Error())
	}

	if wallet.Status == StatusDisabled {
		return domain.Wallet{}, util.NewClientError(map[string]string{"status": "status already disabled"})
	}

	wallet.Status = StatusDisabled
	now := time.Now()
	wallet.DisabledAt = &now
	wallet.EnabledAt = nil

	err = w.walletRepo.Disable(ctx, &wallet)
	if err != nil {
		return domain.Wallet{}, util.NewServerError(err.Error())
	}

	return wallet, err
}

func (w *walletUsecase) GetByID(ctx context.Context, id uuid.UUID) (domain.Wallet, error) {
	wallet, err := w.walletRepo.GetByID(ctx, id)
	if err != nil {
		if err == sql.ErrNoRows {
			return domain.Wallet{}, util.NewClientError(map[string]string{"id": "wallet not found"})
		}
		return domain.Wallet{}, util.NewServerError(err.Error())
	}

	return wallet, err
}

func NewWalletUsecase(wallet domain.WalletRepository, timeout time.Duration) domain.WalletUsecase {
	return &walletUsecase{
		walletRepo:     wallet,
		contextTimeout: timeout,
	}
}
