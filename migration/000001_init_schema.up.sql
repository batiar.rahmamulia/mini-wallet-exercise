CREATE EXTENSION IF NOT EXISTS "uuid-ossp";

CREATE TABLE IF NOT EXISTS accounts(
  id          uuid DEFAULT uuid_generate_v4() PRIMARY KEY,
  owned_by    uuid NOT NULL,
  status      VARCHAR(25) DEFAULT 'disabled' NOT NULL,
  enabled_at  TIMESTAMP,
  disabled_at TIMESTAMP,
  balance     INT DEFAULT 0 NOT NULL
);

CREATE TABLE IF NOT EXISTS transactions(
  id                uuid DEFAULT uuid_generate_v4() PRIMARY KEY,
  transaction_by    uuid NOT NULL,
  status            VARCHAR(25) NOT NULL,
  type              VARCHAR(25) NOT NULL,
  created_at        TIMESTAMP DEFAULT now() NOT NULL,
  amount            INT DEFAULT 0 NOT NULL,
  reference_id      uuid NOT NULL
);