#!/usr/bin/bash
source="../../$2/$1.go"
target="$2/mock_$1/$1_mock.go"
mockgen -source=$source -destination=$target -package=mock_$2
echo "mockgen for file $(pwd)/$source, with destination $target finished"
