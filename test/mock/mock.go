package mock

//go:generate ./mockgen-source.sh wallet domain
//go:generate ./mockgen-source.sh account domain
//go:generate ./mockgen-source.sh transaction domain

//go:generate ./mockgen-source.sh postgresql/wallet repository
//go:generate ./mockgen-source.sh postgresql/transaction repository
