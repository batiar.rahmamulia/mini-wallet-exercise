#!/bin/bash
# Declare variables.
API_URL=http://localhost:8090

# Introduction to the script.
echo "Before running the end-to-end tests, please ensure that you have run 'make api'!"; echo

# Testing '/'.
echo
echo "Running end-to-end testing..."
echo "Testing Health Check route '/'..."
curl $API_URL; echo

# Testing '/api/v1/init'.
echo
echo "Testing Init Wallet route '/api/v1/init'..."
response=$(curl -s -w "\n%{http_code}" -X POST -H 'Content-Type: application/json' -d '{"customer_xid":"a3675c1b-e01c-4034-a3bd-2e049a610efa"}' $API_URL/api/v1/init); echo
http_code=$(tail -n1 <<< "$response");
content=$(echo ${response} | head -c-8)
auth="${content: -235}"
echo $auth

# Testing '/api/v1/wallet'.
echo
echo "Testing Enable Wallet route '/api/v1/wallet'..."
curl -X POST -H 'Content-Type: application/json' -H "Authorization: Bearer $auth" $API_URL/api/v1/wallet; echo
echo
echo "Testing Disable Wallet route '/api/v1/wallet'..."
curl -X PATCH -H 'Content-Type: application/json' -H "Authorization: Bearer $auth" $API_URL/api/v1/wallet; echo
echo
echo "Testing Enable Wallet route '/api/v1/wallet'..."
curl -X POST -H 'Content-Type: application/json' -H "Authorization: Bearer $auth" $API_URL/api/v1/wallet; echo
echo
echo "Testing Get Balance Wallet route '/api/v1/wallet'..."
curl -H "Authorization: Bearer $auth" $API_URL/api/v1/wallet; echo

# Testing '/api/v1/wallet/deposits'.
echo
echo "Testing Deposit Wallet route '/api/v1/wallet/deposits'..."
curl -X POST -H 'Content-Type: application/json' -H "Authorization: Bearer $auth" -d '{"amount": 30000, "reference_id": "50535246-dcb2-4929-8cc9-004ea06f5241"}' $API_URL/api/v1/wallet/deposits; echo


# Testing '/api/v1/wallet/withdrawals'.
echo
echo "Testing Withdrawals Wallet route '/api/v1/wallet/withdrawals'..."
curl -X POST -H 'Content-Type: application/json' -H "Authorization: Bearer $auth" -d '{"amount": 300, "reference_id": "50535246-dcb2-4929-8cc9-004ea06f5241"}' $API_URL/api/v1/wallet/withdrawals; echo

# Finish end-to-end testing.
echo "Finished testing the application!"