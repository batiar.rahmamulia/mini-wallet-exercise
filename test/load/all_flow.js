import http from "k6/http";
import { Rate, Trend } from "k6/metrics";
import { sleep, check } from "k6";
import config from "./config/config.js";
import operation from "./payload/payload.js"

export let errorRate = new Rate("errors");
export let successRate = new Rate("success");
export let initWallet = new Trend("_init", true);
export let disableWallet = new Trend("_disable", true);
export let enableWallet = new Trend("_enable", true);
export let getBalance = new Trend("_balance", true);
export let deposit = new Trend("_deposit", true);
export let withdraw = new Trend("_withdraw", true);

export let options = {
    stages: [
        { duration: '5m', target: 1000 }, // simulate ramp-up of traffic from 1 to 1000 users over 5 minutes.
    ],
    thresholds: {
        success: ["rate>0.95"], // <95% success
        errors: ["rate<0.1"], // <10% errors
        http_req_duration: ["p(95)<500"], // threshold on a standard metric
    },
};

// k6 run --vus 1 --duration 2s test/load/all_flow.js

export default function () {
    let token
    let p = operation.initWallet()
    let resInit = http.post(config.baseUrl+"init", p, config.header);
    initWallet.add(resInit.timings.duration);
    const resultInit = check(resInit, {
        "status is 200": (r) => {
            let body = JSON.parse(r.body)
            if ((r.status >= 200 || r.status < 230) && body.status == "success"){
                token = body.data.token;

                return true
            }

            console.log("init " + JSON.stringify(r.body))
            return false
        }
    });
    if (!resultInit) {
        errorRate.add(!resultInit);
        return
    }

    config.header.headers['Authorization'] ="Bearer " +token

    let resEnable = http.post(config.baseUrl+"wallet", null, config.header);
    enableWallet.add(resEnable.timings.duration);
    var resultEnable = check(resEnable, {
        "status is 200": (r) => {
            let body = JSON.parse(r.body)
            if ((r.status >= 200 || r.status < 230) && body.status == "success"){
                return true
            }
            console.log("enable1 " + JSON.stringify(r.body))
            return false
        }
    });
    if (!resultEnable){
        errorRate.add(!resultEnable);
        return
    }


    let resDisable = http.patch(config.baseUrl+"wallet", null, config.header);
    disableWallet.add(resDisable.timings.duration);
    const resultDisable = check(resDisable, {
        "status is 200": (r) => {
            let body = JSON.parse(r.body)
            if ((r.status >= 200 || r.status < 230) && body.status == "success"){
                return true
            }

            console.log("disable " + JSON.stringify(r.body))
            return false
        }
    });
    if (!resultDisable){
        errorRate.add(!resultDisable);
        return
    }

    resEnable = http.post(config.baseUrl+"wallet", null, config.header);
    enableWallet.add(resEnable.timings.duration);
    resultEnable = check(resEnable, {
        "status is 200": (r) => {
            let body = JSON.parse(r.body)
            if ((r.status >= 200 || r.status < 230) && body.status == "success"){
                return true
            }

            console.log("enable2 " + JSON.stringify(r.body))
            return false
        }
    });
    if (!resultEnable){
        errorRate.add(!resultEnable);
        return
    }

    let resBalance = http.get(config.baseUrl+"wallet", config.header);
    getBalance.add(resBalance.timings.duration);
    const resultBalance = check(resBalance, {
        "status is 200": (r) => {
            let body = JSON.parse(r.body)
            if ((r.status >= 200 || r.status < 230) && body.status == "success"){
                return true
            }

            console.log("balance " +JSON.stringify(r.body))
            return false
        }
    });
    if (!resultBalance){
        errorRate.add(!resultBalance);
        return
    }

    let resDeposit = http.post(config.baseUrl+"wallet/deposits", operation.deposit(), config.header);
    deposit.add(resDeposit.timings.duration);
    const resultDeposit = check(resDeposit, {
        "status is 200": (r) => {
            let body = JSON.parse(r.body)
            if ((r.status >= 200 || r.status < 230) && body.status == "success"){
                return true
            }

            console.log("deposit " + JSON.stringify(r.body))
            return false
        }
    });
    if (!resultDeposit){
        errorRate.add(!resultDeposit);
        return
    }


    let resWithdrawal = http.post(config.baseUrl+"wallet/withdrawals", operation.withdraw(), config.header);
    withdraw.add(resWithdrawal.timings.duration);
    const resultWithdrawal = check(resWithdrawal, {
        "status is 200": (r) => {
            let body = JSON.parse(r.body)
            if ((r.status >= 200 || r.status < 230) && body.status == "success"){
                return true
            }

            console.log("withdraw " + JSON.stringify(r.body))
            return false
        }
    });
    if (resultWithdrawal) {
        successRate.add(resultWithdrawal);
    } else {
        errorRate.add(!resultWithdrawal);
        return
    }

}
