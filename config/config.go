package config

import (
	"database/sql"
	"fmt"
	"log"
	"net/url"
	"time"

	"github.com/spf13/viper"
)

type JWT struct {
	SecretKey                   string `json:"secret_key"`
	SecretKeyExpireMinutesCount int32  `json:"secret_key_expire_minutes_count"`
}

type Server struct {
	Address string `json:"address"`
}

type Context struct {
	Timeout int32 `json:"timeout"`
}

type Database struct {
	Host    string `json:"timeout"`
	Port    string `json:"port"`
	User    string `json:"user"`
	Pass    string `json:"pass"`
	Name    string `json:"name"`
	SSLMode string `json:"sslmode"`
}

type Config struct {
	JWT      JWT      `json:"jwt"`
	Debug    bool     `json:"debug"`
	Server   Server   `json:"server"`
	Context  Context  `json:"context"`
	Database Database `json:"database"`
}

func Init() *Config {
	viper.SetConfigFile("config.json")
	if err := viper.ReadInConfig(); err != nil {
		log.Printf("Fatal error config file: %s\n", err)
	}

	if viper.GetBool(`debug`) {
		log.Println("Service RUN on DEBUG mode")
	}

	conf := &Config{}
	err := viper.Unmarshal(conf)
	if err != nil {
		log.Printf("unable to decode into config struct, %v", err)
	}

	return conf
}

func getPostgresDSN(cfg *Config) (dsn string) {
	connection := fmt.Sprintf("postgres://%s:%s@%s:%s/%s",
		cfg.Database.User,
		cfg.Database.Pass,
		cfg.Database.Host,
		cfg.Database.Port,
		cfg.Database.Name)
	val := url.Values{}
	val.Add("sslmode", cfg.Database.SSLMode)

	dsn = fmt.Sprintf("%s?%s", connection, val.Encode())
	log.Printf("%s", dsn)
	return
}

func Connect(cfg *Config) (*sql.DB, error) {
	dbConn, err := sql.Open(`postgres`, getPostgresDSN(cfg))
	if err != nil {
		return dbConn, err
	}
	err = dbConn.Ping()
	if err != nil {
		return dbConn, err
	}
	dbConn.SetConnMaxLifetime(time.Minute * 5)
	dbConn.SetMaxIdleConns(0)
	dbConn.SetMaxOpenConns(5)

	return dbConn, err
}
