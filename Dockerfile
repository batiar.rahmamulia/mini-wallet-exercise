# Builder
FROM golang:1.18-stretch as builder

COPY . /wallet_api

WORKDIR /wallet_api
ENV GO111MODULE=on

RUN go mod tidy
RUN CGO_ENABLED=0 GOOS=linux go build -o wallet_api cmd/api/*.go


# Distribution
FROM alpine:latest
WORKDIR /root/
RUN apk add --no-cache tzdata
COPY --from=builder /etc/ssl/certs/ca-certificates.crt /etc/ssl/certs/
COPY --from=builder /wallet_api .
EXPOSE 8089
CMD ["./wallet_api"]
