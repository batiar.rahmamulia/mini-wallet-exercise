# Mini Wallet Exercise

## Description

This app server created to solve JULO's Software Engineer Backend Challenge as a part of recruitment.

## Requirement

The app must provide 6 endpoint with this [detail](https://documenter.getpostman.com/view/8411283/SVfMSqA3?version=latest)

## Structure

This app implementing Clean Architecture by Uncle Bob.
There are 4 main folder :
- Domain => same as Entities. This folder will store any Object's Struct and its method.
- Repository => This folder will store any Database handler. Can be extended to other RDBMS, NoSQL, other microservices, etc.
- Usecase => This folder will act as the business process handler.
- Delivery => This folder will act as the presenter. Can be extended to JSON-RPC, gRPC, GraphQL, CLI, etc.

and there are some other folders
- config => to store file config
- test => to store file integration test, load test, end to end test
- cmd => to store main.go file
- migration = to store database file

## Development tools

- [golang-migrate](https://github.com/golang-migrate/migrate): to create db migration
- [mockgen](https://github.com/golang/mock): to generate mock
- [postman](https://www.postman.com/): to call api and simple test
- [k6](https://k6.io/docs/): to run performance test
- [golangci-lint](https://golangci-lint.run/): to run code linter
- [sonarqube](https://www.sonarqube.org/): to maintain code quality and code security
- [make](https://www.gnu.org/software/make/): to simplify all tools call
- Docker + docker compose or local postgres + go 1.18 : to run app as container or local

### How To Run This Project

#### Install Prequisities above

#### Check config/config.json and edit it

#### Create Database on Postgresql

```
Run script on migration/create_database.sql
Run make migrate_up
```

#### Run the UnitTest + IntegrationTest

```
make test
```
#### Push coverage output into sonarqube

```
sonar-scanner
```

#### Run the E2ETest

```
make e2e
```

#### Run the LoadTest

```
make k6_all
```

#### Run the App Local

```
make api
```


#### Other

check make help to check all of tools that I used
```
make help 
```

### For Future Improvements

- Implement jaeger or open telemetry to monitor and troubleshoot
- Implement swagger or open api as API documentation and design tools
- Implement cloud profiler to maintain latency
- Implement monitoring platform like datadog, grafana, prometheus
- Handle idempotency to make sure there is duplication transaction that make wallet balance not accurate
