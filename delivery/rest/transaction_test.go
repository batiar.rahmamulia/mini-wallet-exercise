package rest_test

import (
	"encoding/json"
	"fmt"
	"github.com/gofiber/fiber/v2"
	"github.com/golang/mock/gomock"
	"github.com/google/uuid"
	"github.com/stretchr/testify/assert"
	"mini-wallet-exercise/delivery/rest"
	"mini-wallet-exercise/domain"
	mock_domain "mini-wallet-exercise/test/mock/domain/mock_transaction"
	"net/http/httptest"
	"strings"
	"testing"
)

func TestDeposit(t *testing.T) {
	tests := []struct {
		description  string
		route        string
		expectedCode int
	}{
		{
			description:  "error wrong url",
			route:        "/api/v1/wallet",
			expectedCode: fiber.StatusBadRequest,
		},
		{
			description:  "error",
			route:        "/api/v1/wallet/deposits",
			expectedCode: fiber.StatusBadRequest,
		},
	}

	app := fiber.New()

	mockCtrl := gomock.NewController(t)
	mockTransactionUsecase := mock_domain.NewMockTransactionUsecase(mockCtrl)
	rest.NewTransactionHandler(app, mockTransactionUsecase)

	for _, test := range tests {
		req := httptest.NewRequest(fiber.MethodPost, test.route, nil)
		req.Header.Set(fiber.HeaderContentType, fiber.MIMEApplicationJSON)

		resp, _ := app.Test(req, 10000)

		assert.Equalf(t, test.expectedCode, resp.StatusCode, test.description)
	}
}

func TestWithdrawal(t *testing.T) {
	tests := []struct {
		description  string
		route        string
		expectedCode int
		transaction  domain.Transaction
	}{
		{
			description:  "error wrong url",
			route:        "/api/v1/wallet",
			transaction:  domain.Transaction{},
			expectedCode: fiber.StatusBadRequest,
		},
		{
			description: "error",
			route:       "/api/v1/wallet/withdrawals",
			transaction: domain.Transaction{
				ID:          uuid.UUID{},
				CustomerXid: uuid.UUID{},
				Status:      "",
				CreatedAt:   nil,
				Type:        "",
				Amount:      10,
				ReferenceID: uuid.New(),
			},
			expectedCode: fiber.StatusBadRequest,
		},
		{
			description: "success",
			route:       "/api/v1/wallet/withdrawals",
			transaction: domain.Transaction{
				ID:          uuid.UUID{},
				CustomerXid: uuid.UUID{},
				Status:      "",
				CreatedAt:   nil,
				Type:        "",
				Amount:      10,
				ReferenceID: uuid.New(),
			},
			expectedCode: fiber.StatusBadRequest,
		},
	}

	app := fiber.New()
	mockCtrl := gomock.NewController(t)
	mockTransactionUsecase := mock_domain.NewMockTransactionUsecase(mockCtrl)
	rest.NewTransactionHandler(app, mockTransactionUsecase)

	for _, test := range tests {
		t.Run(fmt.Sprintf("%s", test.description), func(t *testing.T) {
			transactionJson, err := json.Marshal(test.transaction)
			assert.Nil(t, err)
			mockTransactionUsecase.EXPECT().Withdrawal(gomock.Any(), gomock.Any(), gomock.Any()).Return(nil).Times(1)

			req := httptest.NewRequest(fiber.MethodPost, test.route, strings.NewReader(string(transactionJson)))
			req.Header.Set(fiber.HeaderContentType, fiber.MIMEApplicationJSON)

			resp, err := app.Test(req, 10000)

			assert.Equalf(t, test.expectedCode, resp.StatusCode, test.description)
		})
	}
}
