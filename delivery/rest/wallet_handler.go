package rest

import (
	"github.com/gofiber/fiber/v2"
	"github.com/google/uuid"
	"mini-wallet-exercise/delivery/rest/middleware"
	"mini-wallet-exercise/delivery/rest/util"
	"time"

	"mini-wallet-exercise/domain"
)

type WalletHandler struct {
	WalletUsecase domain.WalletUsecase
}

func NewWalletHandler(app *fiber.App, wu domain.WalletUsecase) {
	handler := &WalletHandler{
		WalletUsecase: wu,
	}

	v1 := app.Group("/api/v1")
	wallet := v1.Group("/wallet", middleware.JWTProtected())
	wallet.Post("/", handler.Enable)
	wallet.Get("/", handler.GetByID)
	wallet.Patch("/", handler.Disable)
}

func (w *WalletHandler) Enable(c *fiber.Ctx) error {
	var wallet domain.Wallet

	claims, err := util.ExtractTokenMetadata(c)
	if err != nil {
		return c.Status(fiber.StatusUnauthorized).JSON(
			util.NewResponseFail(map[string]string{"header": "Authorization required"}),
		)
	}

	ctx := c.Context()

	id, err := uuid.Parse(claims.Id)
	if err != nil {
		return c.Status(fiber.StatusUnauthorized).JSON(
			util.NewResponseFail(map[string]string{"header": "Authorization failed"}),
		)
	}

	wallet, err = w.WalletUsecase.Enable(ctx, id)
	if err != nil {
		if err, ok := err.(*util.ClientError); ok {
			return c.Status(fiber.StatusBadRequest).JSON(
				util.NewResponseFail(err.Data),
			)
		}

		return c.Status(fiber.StatusInternalServerError).JSON(
			util.NewResponseError(err.Error()),
		)
	}

	enableRes := struct {
		ID        uuid.UUID  `json:"id"`
		OwnedBy   uuid.UUID  `json:"owned_by"`
		Status    string     `json:"status"`
		EnabledAt *time.Time `json:"enabled_at"`
		Balance   int        `json:"balance"`
	}{
		ID:        wallet.ID,
		OwnedBy:   wallet.OwnedBy,
		Status:    wallet.Status,
		EnabledAt: wallet.EnabledAt,
		Balance:   0,
	}

	return c.Status(fiber.StatusCreated).JSON(
		util.NewResponseSuccess(map[string]interface{}{"wallet": enableRes}),
	)
}

func (w *WalletHandler) Disable(c *fiber.Ctx) error {
	var wallet domain.Wallet

	claims, err := util.ExtractTokenMetadata(c)
	if err != nil {
		return c.Status(fiber.StatusUnauthorized).JSON(
			util.NewResponseFail(map[string]string{"header": "Authorization required"}),
		)
	}

	ctx := c.Context()

	id, err := uuid.Parse(claims.Id)
	if err != nil {
		return c.Status(fiber.StatusUnauthorized).JSON(
			util.NewResponseFail(map[string]string{"header": "Authorization failed"}),
		)
	}
	wallet, err = w.WalletUsecase.Disable(ctx, id)
	if err != nil {
		if err, ok := err.(*util.ClientError); ok {
			return c.Status(fiber.StatusBadRequest).JSON(
				util.NewResponseFail(err.Data),
			)
		}

		return c.Status(fiber.StatusInternalServerError).JSON(
			util.NewResponseError(err.Error()),
		)
	}

	disableRes := struct {
		ID         uuid.UUID  `json:"id"`
		OwnedBy    uuid.UUID  `json:"owned_by"`
		Status     string     `json:"status"`
		DisabledAt *time.Time `json:"disabled_at"`
		Balance    int        `json:"balance"`
	}{
		ID:         wallet.ID,
		OwnedBy:    wallet.OwnedBy,
		Status:     wallet.Status,
		DisabledAt: wallet.DisabledAt,
		Balance:    0,
	}
	return c.Status(fiber.StatusCreated).JSON(
		util.NewResponseSuccess(map[string]interface{}{"wallet": disableRes}),
	)
}

func (w *WalletHandler) GetByID(c *fiber.Ctx) error {
	var wallet domain.Wallet

	claims, err := util.ExtractTokenMetadata(c)
	if err != nil {
		return c.Status(fiber.StatusUnauthorized).JSON(
			util.NewResponseFail(map[string]string{"header": "Authorization required"}),
		)
	}

	ctx := c.Context()

	id, err := uuid.Parse(claims.Id)
	if err != nil {
		return c.Status(fiber.StatusUnauthorized).JSON(
			util.NewResponseFail(map[string]string{"header": "Authorization failed"}),
		)
	}
	wallet, err = w.WalletUsecase.GetByID(ctx, id)
	if err != nil {
		if err, ok := err.(*util.ClientError); ok {
			return c.Status(fiber.StatusBadRequest).JSON(
				util.NewResponseFail(err.Data),
			)
		}

		return c.Status(fiber.StatusInternalServerError).JSON(
			util.NewResponseError(err.Error()),
		)
	}

	return c.Status(fiber.StatusOK).JSON(
		util.NewResponseSuccess(map[string]interface{}{"wallet": wallet}),
	)
}
