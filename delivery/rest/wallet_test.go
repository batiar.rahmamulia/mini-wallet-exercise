package rest_test

import (
	"fmt"
	"github.com/gofiber/fiber/v2"
	"github.com/golang-jwt/jwt/v4"
	"github.com/golang/mock/gomock"
	"github.com/google/uuid"
	"github.com/stretchr/testify/assert"
	"mini-wallet-exercise/delivery/rest"
	"mini-wallet-exercise/domain"
	mock_domain2 "mini-wallet-exercise/test/mock/domain/mock_wallet"
	"net/http/httptest"
	"testing"
	"time"
)

func TestEnable(t *testing.T) {
	now := time.Now()
	tests := []struct {
		description string
		req         struct {
			route  string
			id     uuid.UUID
			method string
		}
		expectedRes struct {
			code int
			err  error
		}
		mockWalletUsecase struct {
			wallet domain.Wallet
			err    error
			call   int
		}
	}{
		{
			description: "error usecase",
			req: struct {
				route  string
				id     uuid.UUID
				method string
			}{route: "/api/v1/wallet", id: uuid.Nil, method: fiber.MethodPost},
			expectedRes: struct {
				code int
				err  error
			}{code: fiber.StatusBadRequest, err: nil},
			mockWalletUsecase: struct {
				wallet domain.Wallet
				err    error
				call   int
			}{
				wallet: domain.Wallet{},
				err:    fmt.Errorf("error"),
				call:   1,
			},
		},
		{
			description: "success",
			req: struct {
				route  string
				id     uuid.UUID
				method string
			}{route: "/api/v1/wallet", id: uuid.Nil, method: fiber.MethodPost},
			expectedRes: struct {
				code int
				err  error
			}{code: fiber.StatusBadRequest, err: nil},
			mockWalletUsecase: struct {
				wallet domain.Wallet
				err    error
				call   int
			}{
				wallet: domain.Wallet{
					ID:         uuid.New(),
					OwnedBy:    uuid.New(),
					Status:     "success",
					EnabledAt:  &now,
					DisabledAt: nil,
					Balance:    10,
				},
				err:  nil,
				call: 1,
			},
		},
	}

	app := fiber.New()
	mockCtrl := gomock.NewController(t)
	mockWalletUsecase := mock_domain2.NewMockWalletUsecase(mockCtrl)

	rest.NewWalletHandler(app, mockWalletUsecase)

	for _, test := range tests {
		t.Run(fmt.Sprintf("%s", test.description), func(t *testing.T) {
			mockWalletUsecase.EXPECT().Enable(gomock.Any(), gomock.Any()).Return(test.mockWalletUsecase.wallet, test.mockWalletUsecase.err).Times(test.mockWalletUsecase.call)

			req := httptest.NewRequest(test.req.method, test.req.route, nil)
			req.Header.Set(fiber.HeaderContentType, fiber.MIMEApplicationJSON)

			claims := jwt.MapClaims{
				"ID":          test.req.id,
				"customerXid": test.req.id,
				"exp":         time.Now().Add(time.Hour * 72).Unix(),
			}
			tkn := jwt.NewWithClaims(jwt.SigningMethodHS256, claims)

			token, err := tkn.SignedString([]byte("secret"))
			assert.Nil(t, err)

			req.Header.Set(fiber.HeaderAuthorization, token)

			resp, err := app.Test(req, 10000)

			assert.Equalf(t, test.expectedRes.code, resp.StatusCode, test.description)
			assert.Equalf(t, test.expectedRes.err, err, test.description)
		})
	}
}
