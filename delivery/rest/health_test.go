package rest_test

import (
	"fmt"
	"github.com/gofiber/fiber/v2"
	"github.com/stretchr/testify/assert"
	"mini-wallet-exercise/delivery/rest"
	"net/http/httptest"
	"testing"
)

func TestMisc(t *testing.T) {
	tests := []struct {
		description  string
		route        string
		expectedCode int
	}{
		{
			description:  "success",
			route:        "/",
			expectedCode: fiber.StatusOK,
		},
	}

	app := fiber.New()

	rest.NewMiscHandler(app)

	for _, test := range tests {
		t.Run(fmt.Sprintf("%s", test.description), func(t *testing.T) {
			req := httptest.NewRequest(fiber.MethodGet, test.route, nil)
			resp, _ := app.Test(req, 10000)
			assert.Equalf(t, test.expectedCode, resp.StatusCode, test.description)
		})
	}
}
