package rest

import (
	"github.com/gofiber/fiber/v2"
	"github.com/google/uuid"
	"mini-wallet-exercise/delivery/rest/middleware"
	"mini-wallet-exercise/delivery/rest/util"
	"mini-wallet-exercise/domain"
	"time"
)

type TransactionHandler struct {
	TransactionUsecase domain.TransactionUsecase
}

func NewTransactionHandler(app *fiber.App, tu domain.TransactionUsecase) {
	handler := &TransactionHandler{
		TransactionUsecase: tu,
	}

	v1 := app.Group("/api/v1")
	wallet := v1.Group("/wallet", middleware.JWTProtected())
	wallet.Post("/deposits", handler.Deposit)
	wallet.Post("/withdrawals", handler.Withdrawal)
}

func (w *TransactionHandler) Deposit(c *fiber.Ctx) error {
	var trans domain.Transaction

	claims, err := util.ExtractTokenMetadata(c)
	if err != nil {
		return c.Status(fiber.StatusUnauthorized).JSON(
			util.NewResponseFail(map[string]string{"header": "Authorization required"}),
		)
	}

	ctx := c.Context()

	id, err := uuid.Parse(claims.Id)
	if err != nil {
		return c.Status(fiber.StatusUnauthorized).JSON(
			util.NewResponseFail(map[string]string{"header": "Authorization failed"}),
		)
	}

	err = c.BodyParser(&trans)
	if err != nil {
		return c.Status(fiber.StatusBadRequest).JSON(
			util.NewResponseFail(map[string]string{"body": "invalid"}),
		)
	}

	err = w.TransactionUsecase.Deposit(ctx, id, &trans)
	if err != nil {
		if err, ok := err.(*util.ClientError); ok {
			return c.Status(fiber.StatusBadRequest).JSON(
				util.NewResponseFail(err.Data),
			)
		}

		return c.Status(fiber.StatusInternalServerError).JSON(
			util.NewResponseError(err.Error()),
		)
	}

	depositRes := struct {
		ID          uuid.UUID  `json:"id"`
		CustomerXid uuid.UUID  `json:"deposited_by"`
		Status      string     `json:"status"`
		CreatedAt   *time.Time `json:"deposited_at"`
		Amount      int        `json:"amount"`
		ReferenceID uuid.UUID  `json:"reference_id"`
	}{
		ID:          trans.ID,
		CustomerXid: trans.CustomerXid,
		Status:      trans.Status,
		CreatedAt:   trans.CreatedAt,
		Amount:      trans.Amount,
		ReferenceID: trans.ReferenceID,
	}

	return c.Status(fiber.StatusCreated).JSON(
		util.NewResponseSuccess(map[string]interface{}{"deposit": depositRes}),
	)
}

func (w *TransactionHandler) Withdrawal(c *fiber.Ctx) error {
	var trans domain.Transaction

	claims, err := util.ExtractTokenMetadata(c)
	if err != nil {
		return c.Status(fiber.StatusUnauthorized).JSON(
			util.NewResponseFail(map[string]string{"header": "Authorization required"}),
		)
	}

	ctx := c.Context()

	id, err := uuid.Parse(claims.Id)
	if err != nil {
		return c.Status(fiber.StatusUnauthorized).JSON(
			util.NewResponseFail(map[string]string{"header": "Authorization failed"}),
		)
	}

	err = c.BodyParser(&trans)
	if err != nil {
		return c.Status(fiber.StatusBadRequest).JSON(
			util.NewResponseFail(map[string]string{"body": "invalid"}),
		)
	}

	mapErrs := trans.Validate()
	if mapErrs != nil {
		return c.Status(fiber.StatusBadRequest).JSON(
			util.NewResponseFail(mapErrs),
		)
	}

	err = w.TransactionUsecase.Withdrawal(ctx, id, &trans)
	if err != nil {
		if err, ok := err.(*util.ClientError); ok {
			return c.Status(fiber.StatusBadRequest).JSON(
				util.NewResponseFail(err.Data),
			)
		}

		return c.Status(fiber.StatusInternalServerError).JSON(
			util.NewResponseError(err.Error()),
		)
	}
	withdrawalRes := struct {
		ID          uuid.UUID  `json:"id"`
		CustomerXid uuid.UUID  `json:"withdrawn_by"`
		Status      string     `json:"status"`
		CreatedAt   *time.Time `json:"withdrawn_at"`
		Amount      int        `json:"amount"`
		ReferenceID uuid.UUID  `json:"reference_id"`
	}{
		ID:          trans.ID,
		CustomerXid: trans.CustomerXid,
		Status:      trans.Status,
		CreatedAt:   trans.CreatedAt,
		Amount:      trans.Amount,
		ReferenceID: trans.ReferenceID,
	}

	return c.Status(fiber.StatusCreated).JSON(
		util.NewResponseSuccess(map[string]interface{}{"withdrawal": withdrawalRes}),
	)
}
