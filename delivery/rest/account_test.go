package rest_test

import (
	"encoding/json"
	"fmt"
	"github.com/gofiber/fiber/v2"
	"github.com/golang/mock/gomock"
	"github.com/google/uuid"
	"github.com/stretchr/testify/assert"
	"mini-wallet-exercise/delivery/rest"
	"mini-wallet-exercise/domain"
	mock_domain "mini-wallet-exercise/test/mock/domain/mock_account"
	"net/http/httptest"
	"strings"
	"testing"
)

func TestCreateAccount(t *testing.T) {
	tests := []struct {
		description string
		req         struct {
			route   string
			account *domain.Account
		}
		expectedRes struct {
			code int
			err  error
		}
		mockAccountUsecase struct {
			token string
			err   error
			call  int
		}
	}{
		{
			description: "invalid param",
			req: struct {
				route   string
				account *domain.Account
			}{route: "/api/v1/init", account: nil},
			expectedRes: struct {
				code int
				err  error
			}{code: fiber.StatusBadRequest, err: nil},
			mockAccountUsecase: struct {
				token string
				err   error
				call  int
			}{token: "", err: nil, call: 0},
		},
		{
			description: "success",
			req: struct {
				route   string
				account *domain.Account
			}{route: "/api/v1/init", account: &domain.Account{CustomerXid: uuid.New()}},
			expectedRes: struct {
				code int
				err  error
			}{code: fiber.StatusCreated, err: nil},
			mockAccountUsecase: struct {
				token string
				err   error
				call  int
			}{token: "initoken", err: nil, call: 1},
		},
	}

	app := fiber.New()
	mockCtrl := gomock.NewController(t)
	mockAccountUsecase := mock_domain.NewMockAccountUsecase(mockCtrl)
	rest.NewAccountHandler(app, mockAccountUsecase)

	for _, test := range tests {
		t.Run(fmt.Sprintf("%s", test.description), func(t *testing.T) {
			accountJson, err := json.Marshal(test.req.account)
			assert.Nil(t, err)
			mockAccountUsecase.EXPECT().Create(gomock.Any(), gomock.Any()).Return(test.mockAccountUsecase.token, test.mockAccountUsecase.err).Times(test.mockAccountUsecase.call)

			req := httptest.NewRequest(fiber.MethodPost, test.req.route, strings.NewReader(string(accountJson)))
			req.Header.Set(fiber.HeaderContentType, fiber.MIMEApplicationJSON)

			resp, err := app.Test(req, 10000)

			assert.Equalf(t, test.expectedRes.code, resp.StatusCode, test.description)
			assert.Equalf(t, test.expectedRes.err, err, test.description)
		})
	}
}
