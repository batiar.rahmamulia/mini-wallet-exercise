package util

type ResponseSuccess struct {
	Status string      `json:"status"`
	Data   interface{} `json:"data"`
}

type ResponseFail struct {
	Status string            `json:"status"`
	Data   map[string]string `json:"data"`
}

type ResponseError struct {
	Status  string `json:"status"`
	Message string `json:"message"`
}

func NewResponseError(message string) *ResponseError {
	return &ResponseError{
		Status:  StatusError,
		Message: message,
	}
}

func NewResponseFail(data map[string]string) *ResponseFail {
	return &ResponseFail{
		Status: StatusFail,
		Data:   data,
	}
}

func NewResponseSuccess(data interface{}) *ResponseSuccess {
	return &ResponseSuccess{
		Status: StatusSuccess,
		Data:   data,
	}
}
