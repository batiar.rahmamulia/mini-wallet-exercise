package util_test

import (
	"fmt"
	"github.com/stretchr/testify/assert"
	"mini-wallet-exercise/delivery/rest/util"
	"testing"
)

func TestResponseError(t *testing.T) {
	tests := []struct {
		description string
		message     string
		expectedRes *util.ResponseError
	}{
		{
			description: "response error",
			message:     "error",
			expectedRes: &util.ResponseError{
				Status:  util.StatusError,
				Message: "error",
			},
		},
	}

	for _, test := range tests {
		t.Run(fmt.Sprintf("%s", test.description), func(t *testing.T) {
			res := util.NewResponseError(test.message)
			assert.Equalf(t, test.expectedRes, res, test.description)
		})
	}
}

func TestResponseFail(t *testing.T) {
	tests := []struct {
		description string
		data        map[string]string
		expectedRes *util.ResponseFail
	}{
		{
			description: "response fail",
			data:        map[string]string{"test": "test"},
			expectedRes: &util.ResponseFail{
				Status: util.StatusFail,
				Data:   map[string]string{"test": "test"},
			},
		},
	}

	for _, test := range tests {
		t.Run(fmt.Sprintf("%s", test.description), func(t *testing.T) {
			res := util.NewResponseFail(test.data)
			assert.Equalf(t, test.expectedRes, res, test.description)
		})
	}
}

func TestResponseSuccess(t *testing.T) {
	tests := []struct {
		description string
		data        interface{}
		expectedRes *util.ResponseSuccess
	}{
		{
			description: "response error",
			data:        "error",
			expectedRes: &util.ResponseSuccess{
				Status: util.StatusSuccess,
				Data:   "error",
			},
		},
	}

	for _, test := range tests {
		t.Run(fmt.Sprintf("%s", test.description), func(t *testing.T) {
			res := util.NewResponseSuccess(test.data)
			assert.Equalf(t, test.expectedRes, res, test.description)
		})
	}
}
