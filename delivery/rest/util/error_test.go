package util_test

import (
	"fmt"
	"github.com/stretchr/testify/assert"
	"mini-wallet-exercise/delivery/rest/util"
	"testing"
)

func TestError(t *testing.T) {
	tests := []struct {
		description     string
		message         string
		data            map[string]string
		expectedErr     error
		expectedMessage string
	}{
		{
			description:     "client error",
			data:            map[string]string{"body": "error"},
			expectedMessage: fmt.Sprintf("[body]: error"),
			expectedErr:     &util.ClientError{Data: map[string]string{"body": "error"}},
		},
		{
			description:     "server error",
			message:         "error",
			expectedMessage: fmt.Sprintf("error"),
			expectedErr:     &util.ServerError{Message: "error"},
		},
	}

	for _, test := range tests {
		t.Run(fmt.Sprintf("%s", test.description), func(t *testing.T) {
			var err error
			var message string
			if test.data == nil {
				err = util.NewServerError(test.message)
				message = err.Error()
			} else {
				err = util.NewClientError(test.data)
				message = err.Error()
			}
			assert.Equalf(t, test.expectedErr, err, test.description)
			assert.Equalf(t, test.expectedMessage, message, test.description)

		})
	}

}
