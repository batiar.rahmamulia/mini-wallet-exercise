package util

import (
	"fmt"
)

const (
	StatusSuccess = "success"
	StatusFail    = "fail"
	StatusError   = "error"
)

type ClientError struct {
	Data map[string]string `json:"data"`
}

func (f *ClientError) Error() string {
	var message string
	for idx, val := range f.Data {
		return fmt.Sprintf("[%v]: %v", idx, val)

	}
	return message
}

func NewClientError(data map[string]string) error {
	return &ClientError{
		Data: data,
	}
}

type ServerError struct {
	Message string `json:"message"`
}

func (f *ServerError) Error() string {
	return fmt.Sprintf("%v", f.Message)
}

func NewServerError(message string) error {
	return &ServerError{
		Message: message,
	}
}
