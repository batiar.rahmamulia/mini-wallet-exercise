package rest

import (
	"github.com/gofiber/fiber/v2"
	"mini-wallet-exercise/delivery/rest/util"
	"mini-wallet-exercise/domain"
)

type DataToken struct {
	Token string `json:"token"`
}

type AccountHandler struct {
	AccountUsecase domain.AccountUsecase
}

func NewAccountHandler(app *fiber.App, au domain.AccountUsecase) {
	handler := &AccountHandler{
		AccountUsecase: au,
	}

	v1 := app.Group("/api/v1")
	v1.Post("/init", handler.CreateAccount)
}
func (a *AccountHandler) CreateAccount(c *fiber.Ctx) error {
	var account domain.Account

	err := c.BodyParser(&account)
	if err != nil {
		return c.Status(fiber.StatusBadRequest).JSON(
			util.NewResponseFail(map[string]string{"body": "invalid"}),
		)
	}

	mapErrs := account.Validate()
	if mapErrs != nil {
		return c.Status(fiber.StatusBadRequest).JSON(
			util.NewResponseFail(mapErrs),
		)
	}

	ctx := c.Context()

	token, err := a.AccountUsecase.Create(ctx, &account)
	if err != nil {
		if err, ok := err.(*util.ClientError); ok {
			return c.Status(fiber.StatusBadRequest).JSON(
				util.NewResponseFail(err.Data),
			)
		}

		return c.Status(fiber.StatusInternalServerError).JSON(
			util.NewResponseError(err.Error()),
		)
	}

	return c.Status(fiber.StatusCreated).JSON(
		util.NewResponseSuccess(DataToken{
			Token: token,
		}),
	)
}
